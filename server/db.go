package server

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"os"
	"fmt"
)

var db *gorm.DB

func init() {
	println("open db...")
	user := os.Getenv("MYSQL_USER")
	passwd := os.Getenv("MYSQL_PASSWORD")
	dbname := os.Getenv("MYSQL_DATABASE")
	d, err := gorm.Open("mysql", fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", user, passwd, dbname))
	if err != nil {
		panic(err)
	}
	d.AutoMigrate(&User{})
	db = d
}

type User struct {
	gorm.Model
	Name string `gorm:"type:varchar(100);not null;unique"`
	Passwd string `gorm:"size:100;not null"`
	Age int
}
