package server

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func helloHandler(c *gin.Context) {
	db.Create(&User{Name:"zcong", Age:19})
	c.JSON(http.StatusOK, gin.H{
		"status": "ok",
		"msg": "hello world",
	})
}

func findUser(c *gin.Context) {
	name := c.Param("name")
	var user User
	db.Where("name = ?", name).First(&user)
	if user.Name != name {
		println("not found")
		c.JSON(http.StatusOK, gin.H{
			"status": "not founc",
			"msg": "user not found",
		})
		return
	}
	c.JSON(http.StatusOK, user)
}

func GinEngine() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.GET("/", helloHandler)
	r.GET("/user/:name", findUser)
	return r
}
