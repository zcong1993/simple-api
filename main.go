package main

import "gitlab.com/zcong1993/simple-api/server"

func main() {
	s := server.GinEngine()
	s.Run(":8000")
}
